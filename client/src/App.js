import React, { Component } from 'react';
import './App.css';
import Autosearch from './components/Autocomplete/Autosearch';
import Autosearchcost from './components/Autocomplete/Autosearchcost';
import PrivateRoute from './components/comman/privateRoute';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Register from './components/auth/register';
import Login from './components/auth/login';
import Navbar from './components/Layout/Navbar';
import Report from './components/Autocomplete/Autocomplete/Reports/ReportItem';

class App extends Component {
  render() {
    return (
      <Router>
        <Navbar />
        <Route exact path='/register' component={Register} />
        <Route exact path='/' component={Login} />
        <Switch>
          <PrivateRoute exact path='/auto' component={Autosearch}/>          
        </Switch>
        <Switch>
          <PrivateRoute exact path='/autocost' component={Autosearchcost}/>          
        </Switch>
        <Switch>
          <PrivateRoute exact path='/selected_report' component={Autosearch}
          />
        </Switch>
        <Switch>
          <PrivateRoute exact path='/auto/:id' component={Report} />
        </Switch>
      </Router>
    );
  }
}

export default App;
