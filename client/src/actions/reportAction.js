import { GET_ERROR, GET_REPORT, GET_REPORT_DATA } from './types';
import axios from 'axios';

export const getReportData = () => dispatch => {
  axios
    .get('/report/all')
    .then(res =>
      dispatch({
        type: GET_REPORT,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: {}
      })
    );
}

export const getReportDataCost = () => dispatch => {
  axios
    .get('/report/allcost')
    .then(res =>
      dispatch({
        type: GET_REPORT,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: {}
      })
    );
}

export const selectedReportData = (report_data, history) => dispatch => {
  axios
    .post('/report/keyword_search', { title: report_data })
    .then(res => {
      console.log(res)
      return dispatch({
        type: GET_REPORT,
        payload: res.data
      })
    }

    )
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: {}
      })
    );
}


export const getReport = (data) => dispatch => {
  axios
    .post(`/report/title/${data}`)
    .then(res => dispatch({
      type: GET_REPORT_DATA,
      payload: res.data
    })

    )
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: {}
      })
    );
}


