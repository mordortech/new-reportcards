export const GET_ERROR = 'GET_ERROR';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const GET_REPORT = 'GET_REPORT';
export const REPORT_LOADING = 'REPORT_LOADING';
export const GET_REPORT_DATA = 'GET_REPORT_DATA';