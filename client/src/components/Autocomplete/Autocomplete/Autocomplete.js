import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import './Autocomplete.css';
import { selectedReportData } from '../../../actions/reportAction';
import { connect } from "react-redux";

class Autocomplete extends Component {
  static propTypes = {
    suggestions: PropTypes.instanceOf(Array)
  };

  static defaultProps = {
    suggestions: []
  };

  constructor(props) {
    super(props);

    this.state = {
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: ""
    };
  }


  onChange = e => {
    let suggestions = [];
    suggestions = this.props.suggestions;
    const userInput = e.currentTarget.value;
    console.log(e.currentTarget.value);

    const filteredSuggestions = suggestions.filter(
      suggestion => {

        return suggestion.toLowerCase().indexOf(userInput.toLowerCase()) === 0;
      }

    );


    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    });
  };

  onClick = e => {

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });

  };


  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;


    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion]

      });
    }

    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }

    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };
  submitHandler(e) {
    e.preventDefault();

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });

    console.log(this.state.userInput);
    this.props.selectedReportData(this.state.userInput, this.props.history);


  }
  render() {

    const {
      onChange,
      onClick,
      onKeyDown,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput
      }
    } = this;

    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              let className;


              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li
                  className={className}
                  key={suggestion}
                  onClick={onClick}
                >
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">
            <em>No suggestions, you're on your own!</em>
          </div>
        );
      }
    }

    return (
      <Fragment>
        <form noValidate onSubmit={(e) => this.submitHandler(e)}>
          <input
            type="text"
            onChange={onChange}
            onKeyDown={onKeyDown}
            value={userInput}
            style={{ marginLeft: "490px" }}
          /><br></br>

          {suggestionsListComponent}
          <input type="submit" style={{ marginLeft: "490px" }} className="btn btn-primary  mt-4" />
        </form>

      </Fragment>
    );
  }
}


const mapStateToProps = state => ({
  report: state.report,
  auth: state.auth
})

export default connect(mapStateToProps, { selectedReportData })(Autocomplete);
