import React, { Component } from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import Spinner from '../../../Spinner/spinner';
import { getReportData } from '../../../../actions/reportAction';
import ReportListItem from './ReportListItem';

class Filter_Report extends Component {
    render() {
        const { report, loading } = this.props.report;
        const { auth } = this.props;
        let reportListItem;
        if (report === null || loading) {
            reportListItem = <Spinner />;
        }
        else {
            if (Object.keys(report).length > 0) {
                reportListItem = report.map(report => (

                    <ReportListItem key={report._id} report={report} auth={auth} />
                    
                ));
            } else {
                reportListItem = <h1>Report was not found...!</h1>
            }
        }
        return (

            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1 className="display-4 text-center">All Reports</h1>

                        {reportListItem}

                    </div>
                </div>
            </div>

        )
    }
}

Filter_Report.propTypes = {
    getReportData: propTypes.func.isRequired,
    auth: propTypes.object.isRequired
}
const mapStateToProps = state => ({
    report: state.report,
    auth: state.auth
})
export default connect(mapStateToProps, { getReportData })(Filter_Report);