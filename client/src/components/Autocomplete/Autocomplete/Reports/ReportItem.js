import React, { Component } from 'react';
import { getReport } from "../../../../actions/reportAction";
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import Spinner from '../../../Spinner/spinner';
import { Link } from 'react-router-dom';

class ReportItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }
  componentDidMount() {

    this.props.getReport(this.props.match.params.id);
    this.setState({ loading: false })
  }

  render() {
    let check;

    if (this.props.report.reportdata === null) {
      check = <Spinner />
    }

    else {
      const report = this.props.report.reportdata;
      const dates = new Date(report.date);
      let dated = dates.getFullYear() + '-' + (dates.getMonth() + 1) + '-' + dates.getDate();
      check = <div className="card card-body bg-light mb-3">

        <div className="row">

          <div className="col-md-4">
            <img src={report.image} alt="" style={{ width: "200px", height: "200px" }} />

          </div>
          <div className="col-lg-6 col-md-6 col-6">
            <p>title:</p>

            <h1>{report.title}</h1>

            <p>
              description:{report.description}{' '}
            </p>
            <p>
              Cost:
                      <span>{report.cost}</span>

            </p>


            
          </div>
          <div className="col-md-2 d-none d-md-block">
            <h4>Published:</h4> <br></br><h2>{dated}</h2>


          </div>

        </div>

      </div>;
    }

    return (
      <div><Link to="/auto" className="btn btn-light mb-3 float-left">
        Back To Reports
  </Link>{check}</div>
    )
  }
}

ReportItem.propTypes = {
  getReport: propTypes.func.isRequired,
  report: propTypes.object.isRequired
}
const mapStateToProps = state => ({
  report: state.report
})

export default connect(mapStateToProps, { getReport })(ReportItem);