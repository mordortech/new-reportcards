import React, { Component } from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './ReportListItem.css';

class ReportListItem extends Component {

  render() {
    const { report } = this.props;
    const dates = new Date(report.date);
    let dated = dates.getFullYear() + '-' + (dates.getMonth() + 1) + '-' + dates.getDate();
    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">
          <div className="col-md-4">
            <img src={report.image} alt="" style={{ width: "200px", height: "200px" }} />

          </div>
          <div className="col-lg-6 col-md-6 col-6">
            <p></p>
            <Link to={`/auto/${report._id}`} className="btn btn-success">
              {report.title}
            </Link>
            <h1>
              {report.cost}{' '}$
                  </h1>
          </div>
          <div className="col-md-2 d-none d-md-block">
            <h4>Published:</h4> <br></br><h2>{dated}</h2>


          </div>

        </div>

      </div>
    );
  }
}

ReportListItem.propTypes = {
  report: propTypes.object.isRequired,

};

export default ReportListItem;