import React, { Component } from 'react'
import Autocomplete from './Autocomplete/Autocomplete';
import ReportList from './Autocomplete/Reports/ReportList';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Autosearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: [],
            loading: true
        }
    }
    componentDidMount() {
        axios.get('/report/title')
            .then(res => this.setState({ title: res.data, loading: false }))

    }
    render() {
        let title;
        if (this.state.loading) {
            title = [

            ]
        } else {
            title = this.state.title;
        }
        return (
            <div>

                <Autocomplete
                    suggestions={title}

                ></Autocomplete>
                <Link to='/autocost'>Filter Via Cost</Link>
                <ReportList />
            </div>
        )
    }
}

export default Autosearch;