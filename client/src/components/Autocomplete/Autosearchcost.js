import React, { Component } from 'react'
import Autocomplete from './Autocomplete/Autocomplete';
import ReportList from './Autocomplete/Reports/ReportListCost';
import axios from 'axios';

class Autosearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: [],
            loading: true
        }
    }
    componentDidMount() {
        axios.get('/report/title')
            .then(res => this.setState({ title: res.data, loading: false }))

    }
    render() {
        let title;
        if (this.state.loading) {
            title = [

            ]
        } else {
            title = this.state.title;
        }
        return (
            <div>

                <Autocomplete
                    suggestions={title}

                ></Autocomplete>
                <ReportList />
            </div>
        )
    }
}

export default Autosearch;