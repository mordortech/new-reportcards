import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import reportReducer from './reportReducer';




export default combineReducers({
    auth: authReducer,
    report: reportReducer,
    error: errorReducer
});