import { GET_REPORT, REPORT_LOADING, GET_REPORT_DATA } from '../actions/types';

const initialState = {
    report: null,
    loading: false,
    reportdata: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case
            REPORT_LOADING: return {
                ...state,
                loading: true
            }
        case
            GET_REPORT: return {
                ...state,
                report: action.payload,
                loading: false
            }
        case GET_REPORT_DATA: return {
            ...state,
            reportdata: action.payload,
        }
        default:
            return state;
    }
}

export default reducer;