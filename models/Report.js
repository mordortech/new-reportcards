const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReportSchema = Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    id: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    cost: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        required: true,
    }
});

const Report = mongoose.model('report', ReportSchema);
module.exports = Report;