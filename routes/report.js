const express = require('express');
const router = express.Router();
const passport = require('passport');
const Card = require('../models/Card');
const Report = require('../models/Report');

router.get('/all', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const report = await Card.find().sort({ date: -1 });
        if (report) {
            return res.status(200).json(report);
        } else {
            return res.status(404).json({ error: "not found" });
        }
    } catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }

});

router.get('/allcost', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const report = await Card.find().sort({ cost: 1 });
        if (report) {
            return res.status(200).json(report);
        } else {
            return res.status(404).json({ error: "not found" });
        }
    } catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }

});


router.post('/keyword_search', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const title = await Card.find().select('title');
        let selected_title = [];
        let choose;

        for (let i = 0; i < title.length; i++) {

            if (title[i].title.startsWith(req.body.title)) {

                choose = await Card.find({ title: title[i].title });

                selected_title.push(choose[0]);
            }
        }
        return res.status(200).json(selected_title);
    }
    catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }

});


router.post('/title/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const Report_history = new Report();
        Report_history.user = req.user.id;
        const verify_report = await Report.findOne({ user: req.user.id, id: req.params.id });
        if (verify_report) {
            return res.status(200).json(verify_report);
        } else {
            const report = await Card.findOne({ _id: req.params.id });
            Report_history.id = req.params.id;
            Report_history.title = report.title;
            Report_history.image = report.image;
            Report_history.description = report.description;
            Report_history.cost = report.cost;
            Report_history.date = report.date;

            const reports = await Report_history.save()
            if (reports) {
                return res.status(200).json(reports);
            } else {
                return res.status(404).json({ error: "not found" });
            }
        }
    } catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }


});

router.get('/title', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const title = await Card.find().select('title');
        let selected_title = [];
        for (let i = 0; i < title.length; i++) {
            selected_title.push(title[i].title);
        }
        return res.status(200).json(selected_title);
    } catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }
})

router.get('/search_data', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const report_history = await Report.find({ user: req.user.id });
        if (report_history) {
            return res.json(report_history);
        }
    } catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }
});


module.exports = router;