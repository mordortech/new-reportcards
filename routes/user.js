const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');
const User = require('../models/User');


router.post('/register', async (req, res) => {
    try {
        const { error, isValid } = validateRegisterInput(req.body);
        if (!isValid) {
            return res.status(400).json(error);
        }

        const user = await User.findOne({ email: req.body.email })

        if (user) {
            return res.status(404).json({ email: "that email is already exists" });
        }
        else {

            const newUser = new User({
                name: req.body.name,
                email: req.body.email,

                password: req.body.password
            });
            bcrypt.genSalt(10, async (err, salt) => {
                bcrypt.hash(req.body.password, salt, async (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    const users = await newUser.save()
                    if (users) {
                        return res.status(200).json(users);
                    }
                });
            });
        }
    } catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }

});

router.post('/login', async (req, res) => {
    try {
        const { error, isValid } = validateLoginInput(req.body);
        if (!isValid) {
            return res.status(400).json(error);
        }

        const email = req.body.email;
        const password = req.body.password;
        const user = await User.findOne({ email });

        if (!user) {
            return res.status(404).json({ email: "email not exists" });
        }
        const isMatch = await bcrypt.compare(password, user.password)

        if (isMatch) {


            const payload = { id: user.id, name: user.name, avatar: user.avatar };
            jwt.sign(payload, keys.secretOrKey, { expiresIn: 360000 },
                (err, token) => {
                    res.status(200).json({
                        success: true,
                        token: 'bearer ' + token
                    })
                })
        }
        else {
            error.password = 'Password incorrect';
            return res.status(404).json(error);
        }

    } catch (e) {
        return res.status(400).json({ error: "somthing wrong" });
    }

});


module.exports = router;