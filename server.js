const express = require('express');
const app = express();
const db = require('./config/keys').mongoUrl;
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = require('./routes/user');
const Report = require('./routes/report');
const passport = require('passport');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect(db, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => console.log('MongoDB Connected...'))
  .catch(err => console.log(err));


app.use(passport.initialize());
require('./config/passport')(passport);

app.use('/user', User);
app.use('/report', Report);
app.get('/', (req, res) => {
  res.json({ s: "start" });
});
// app.get('/post',async (req,res)=>{
//     const image=await axios.get('https://api.unsplash.com/photos/?client_id=2c0a37d7e38c0074459e65b7a81d0f7b51ae900726f862ed7bcfe64d41246091');
//     //console.log(image.data);
//     const imagefilter=image.data.map((value)=>{
//         const hel={};

//         date = new Date(value.created_at);
//         let dated=date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
//         hel.date=dated;
//         hel.description=value.description+value.user.bio;
//         hel.cost=value.width;
//         hel.title=value.user.username;
//         hel.image=value.urls.small;
//         return hel;
//     })
//     //return res.json(imagefilter)
//     //console.log(imagefilter);
//     // Card.save(imagefilter)
//     // .then(p=>{
//     //     console.log("ok");
//     // })
//     // let datas=new Card();
//     // for(let i=0;i<imagefilter.length;i++){
//     //     datas=imagefilter[i];
//     //     datas.save();
//     // }
//     for(let i=0;i<imagefilter.length;i++){
//         const hel=new Card({
//             title:imagefilter[i].title,
//             image:imagefilter[i].image,
//             description:imagefilter[i].description,
//             cost:imagefilter[i].cost,
//             date:imagefilter[i].date,
//         });
//         hel.save();
//     }

//     console.log(imagefilter[0])
//     //return res.json(JSON.stringify(image));
// });


const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`server connected at ${port}`);
});